# RadiumPHP
## Developing for the Marie Curie website

---

## Getting Started

The RadiumPHP framework for the Marie Curie website is a **MVC** based framework developed in-house.

@size[0.5em](
- A *M*odel is a piece of data.
- A *V*iew is output html for the user.
- A *C*ontroller is the glue code that takes input, loads models and selects one or more views.
)

---

## Utilities

Outside **MVC** there are several other types of objects in RadiumPHP framework

- The are *Widgets*, little page elements like the birthday calendar
- *Layouts* are special controllers that can be inherited that set up the base site layout
- *Helpers* can be anything to help you out

